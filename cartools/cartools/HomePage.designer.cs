﻿namespace login
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.categoryManagementToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customManagementToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.carManagementToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bookingManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoryManagementToolStripMenuItem1,
            this.customManagementToolStripMenuItem1,
            this.carManagementToolStripMenuItem1,
            this.bookingManagementToolStripMenuItem,
            this.customReportToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(840, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // categoryManagementToolStripMenuItem1
            // 
            this.categoryManagementToolStripMenuItem1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.categoryManagementToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.categoryManagementToolStripMenuItem1.Name = "categoryManagementToolStripMenuItem1";
            this.categoryManagementToolStripMenuItem1.Size = new System.Drawing.Size(183, 24);
            this.categoryManagementToolStripMenuItem1.Text = "Category Management";
            // 
            // customManagementToolStripMenuItem1
            // 
            this.customManagementToolStripMenuItem1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.customManagementToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.customManagementToolStripMenuItem1.Name = "customManagementToolStripMenuItem1";
            this.customManagementToolStripMenuItem1.Size = new System.Drawing.Size(188, 24);
            this.customManagementToolStripMenuItem1.Text = "Customer Management";
            this.customManagementToolStripMenuItem1.Click += new System.EventHandler(this.customManagementToolStripMenuItem1_Click);
            // 
            // carManagementToolStripMenuItem1
            // 
            this.carManagementToolStripMenuItem1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.carManagementToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.carManagementToolStripMenuItem1.Name = "carManagementToolStripMenuItem1";
            this.carManagementToolStripMenuItem1.Size = new System.Drawing.Size(144, 24);
            this.carManagementToolStripMenuItem1.Text = "Car Management";
            this.carManagementToolStripMenuItem1.Click += new System.EventHandler(this.carManagementToolStripMenuItem1_Click);
            // 
            // bookingManagementToolStripMenuItem
            // 
            this.bookingManagementToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bookingManagementToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.bookingManagementToolStripMenuItem.Name = "bookingManagementToolStripMenuItem";
            this.bookingManagementToolStripMenuItem.Size = new System.Drawing.Size(177, 24);
            this.bookingManagementToolStripMenuItem.Text = "Booking Management";
            // 
            // customReportToolStripMenuItem
            // 
            this.customReportToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.customReportToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.customReportToolStripMenuItem.Name = "customReportToolStripMenuItem";
            this.customReportToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.customReportToolStripMenuItem.Text = "Custom Report";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(840, 475);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomePage";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem categoryManagementToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customManagementToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem carManagementToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bookingManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customReportToolStripMenuItem;
    }
}