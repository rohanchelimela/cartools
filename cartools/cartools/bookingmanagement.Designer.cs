﻿namespace cartools
{
    partial class bookingmanagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnbook = new MyControls.MyButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.crid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rntdt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rtndt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tamt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnfind = new MyControls.MyButton();
            this.btndelete = new MyControls.MyButton();
            this.btnupdate = new MyControls.MyButton();
            this.btnsave = new MyControls.MyButton();
            this.myTextBox2 = new MyControls.MyTextBox();
            this.myTextBox1 = new MyControls.MyTextBox();
            this.myComboBox4 = new MyControls.MyComboBox();
            this.myComboBox3 = new MyControls.MyComboBox();
            this.myComboBox2 = new MyControls.MyComboBox();
            this.myComboBox1 = new MyControls.MyComboBox();
            this.myLabel6 = new MyControls.MyLabel();
            this.myLabel5 = new MyControls.MyLabel();
            this.myLabel4 = new MyControls.MyLabel();
            this.myLabel3 = new MyControls.MyLabel();
            this.myLabel2 = new MyControls.MyLabel();
            this.myLabel1 = new MyControls.MyLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnbook
            // 
            this.btnbook.AutoSize = true;
            this.btnbook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnbook.FlatAppearance.BorderSize = 2;
            this.btnbook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnbook.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnbook.Location = new System.Drawing.Point(300, 473);
            this.btnbook.Name = "btnbook";
            this.btnbook.Size = new System.Drawing.Size(96, 30);
            this.btnbook.TabIndex = 35;
            this.btnbook.Text = "BOOK";
            this.btnbook.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Highlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.crid,
            this.ctid,
            this.rntdt,
            this.rtndt,
            this.tamt});
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView1.Location = new System.Drawing.Point(75, 296);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(538, 150);
            this.dataGridView1.TabIndex = 34;
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            // 
            // crid
            // 
            this.crid.HeaderText = "Car ID";
            this.crid.Name = "crid";
            // 
            // ctid
            // 
            this.ctid.HeaderText = "Customer ID";
            this.ctid.Name = "ctid";
            // 
            // rntdt
            // 
            this.rntdt.HeaderText = "Rent Date";
            this.rntdt.Name = "rntdt";
            // 
            // rtndt
            // 
            this.rtndt.HeaderText = "Return Date";
            this.rtndt.Name = "rtndt";
            // 
            // tamt
            // 
            this.tamt.HeaderText = "Total Amount";
            this.tamt.Name = "tamt";
            // 
            // btnfind
            // 
            this.btnfind.AutoSize = true;
            this.btnfind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnfind.FlatAppearance.BorderSize = 2;
            this.btnfind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnfind.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnfind.Location = new System.Drawing.Point(517, 240);
            this.btnfind.Name = "btnfind";
            this.btnfind.Size = new System.Drawing.Size(96, 30);
            this.btnfind.TabIndex = 33;
            this.btnfind.Text = "Find";
            this.btnfind.UseVisualStyleBackColor = false;
            // 
            // btndelete
            // 
            this.btndelete.AutoSize = true;
            this.btndelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btndelete.FlatAppearance.BorderSize = 2;
            this.btndelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btndelete.ForeColor = System.Drawing.Color.GhostWhite;
            this.btndelete.Location = new System.Drawing.Point(373, 240);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(96, 30);
            this.btndelete.TabIndex = 32;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = false;
            // 
            // btnupdate
            // 
            this.btnupdate.AutoSize = true;
            this.btnupdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnupdate.FlatAppearance.BorderSize = 2;
            this.btnupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnupdate.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnupdate.Location = new System.Drawing.Point(224, 240);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(96, 30);
            this.btnupdate.TabIndex = 31;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = false;
            // 
            // btnsave
            // 
            this.btnsave.AutoSize = true;
            this.btnsave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnsave.FlatAppearance.BorderSize = 2;
            this.btnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnsave.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnsave.Location = new System.Drawing.Point(75, 240);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(96, 30);
            this.btnsave.TabIndex = 30;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = false;
            // 
            // myTextBox2
            // 
            this.myTextBox2.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox2.Location = new System.Drawing.Point(156, 147);
            this.myTextBox2.Name = "myTextBox2";
            this.myTextBox2.Size = new System.Drawing.Size(145, 29);
            this.myTextBox2.TabIndex = 29;
            // 
            // myTextBox1
            // 
            this.myTextBox1.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox1.Location = new System.Drawing.Point(504, 147);
            this.myTextBox1.Name = "myTextBox1";
            this.myTextBox1.ReadOnly = true;
            this.myTextBox1.Size = new System.Drawing.Size(145, 29);
            this.myTextBox1.TabIndex = 28;
            // 
            // myComboBox4
            // 
            this.myComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox4.FormattingEnabled = true;
            this.myComboBox4.Location = new System.Drawing.Point(504, 86);
            this.myComboBox4.Name = "myComboBox4";
            this.myComboBox4.Size = new System.Drawing.Size(145, 32);
            this.myComboBox4.TabIndex = 27;
            this.myComboBox4.Text = "--Select--";
            // 
            // myComboBox3
            // 
            this.myComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox3.FormattingEnabled = true;
            this.myComboBox3.Location = new System.Drawing.Point(156, 94);
            this.myComboBox3.Name = "myComboBox3";
            this.myComboBox3.Size = new System.Drawing.Size(145, 32);
            this.myComboBox3.TabIndex = 26;
            this.myComboBox3.Text = "--Select--";
            // 
            // myComboBox2
            // 
            this.myComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox2.FormattingEnabled = true;
            this.myComboBox2.Location = new System.Drawing.Point(156, 27);
            this.myComboBox2.Name = "myComboBox2";
            this.myComboBox2.Size = new System.Drawing.Size(145, 32);
            this.myComboBox2.TabIndex = 25;
            this.myComboBox2.Text = "--Select--";
            // 
            // myComboBox1
            // 
            this.myComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox1.FormattingEnabled = true;
            this.myComboBox1.Location = new System.Drawing.Point(504, 22);
            this.myComboBox1.Name = "myComboBox1";
            this.myComboBox1.Size = new System.Drawing.Size(145, 32);
            this.myComboBox1.TabIndex = 24;
            this.myComboBox1.Text = "--Select--";
            // 
            // myLabel6
            // 
            this.myLabel6.AutoSize = true;
            this.myLabel6.BackColor = System.Drawing.Color.Transparent;
            this.myLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel6.Location = new System.Drawing.Point(339, 94);
            this.myLabel6.Name = "myLabel6";
            this.myLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.myLabel6.Size = new System.Drawing.Size(109, 24);
            this.myLabel6.TabIndex = 23;
            this.myLabel6.Text = "Return Date";
            // 
            // myLabel5
            // 
            this.myLabel5.AutoSize = true;
            this.myLabel5.BackColor = System.Drawing.Color.Transparent;
            this.myLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel5.Location = new System.Drawing.Point(339, 152);
            this.myLabel5.Name = "myLabel5";
            this.myLabel5.Size = new System.Drawing.Size(122, 24);
            this.myLabel5.TabIndex = 22;
            this.myLabel5.Text = "Total Amount";
            // 
            // myLabel4
            // 
            this.myLabel4.AutoSize = true;
            this.myLabel4.BackColor = System.Drawing.Color.Transparent;
            this.myLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel4.Location = new System.Drawing.Point(339, 30);
            this.myLabel4.Name = "myLabel4";
            this.myLabel4.Size = new System.Drawing.Size(148, 24);
            this.myLabel4.TabIndex = 21;
            this.myLabel4.Text = "Select Customer";
            // 
            // myLabel3
            // 
            this.myLabel3.AutoSize = true;
            this.myLabel3.BackColor = System.Drawing.Color.Transparent;
            this.myLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel3.Location = new System.Drawing.Point(37, 94);
            this.myLabel3.Name = "myLabel3";
            this.myLabel3.Size = new System.Drawing.Size(92, 24);
            this.myLabel3.TabIndex = 20;
            this.myLabel3.Text = "Rent Date";
            // 
            // myLabel2
            // 
            this.myLabel2.AutoSize = true;
            this.myLabel2.BackColor = System.Drawing.Color.Transparent;
            this.myLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel2.Location = new System.Drawing.Point(37, 150);
            this.myLabel2.Name = "myLabel2";
            this.myLabel2.Size = new System.Drawing.Size(106, 24);
            this.myLabel2.TabIndex = 19;
            this.myLabel2.Text = "No. of Days";
            // 
            // myLabel1
            // 
            this.myLabel1.AutoSize = true;
            this.myLabel1.BackColor = System.Drawing.Color.Transparent;
            this.myLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel1.Location = new System.Drawing.Point(37, 30);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(96, 24);
            this.myLabel1.TabIndex = 18;
            this.myLabel1.Text = "Select Car";
            // 
            // bookingmanagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 528);
            this.Controls.Add(this.btnbook);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnfind);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.myTextBox2);
            this.Controls.Add(this.myTextBox1);
            this.Controls.Add(this.myComboBox4);
            this.Controls.Add(this.myComboBox3);
            this.Controls.Add(this.myComboBox2);
            this.Controls.Add(this.myComboBox1);
            this.Controls.Add(this.myLabel6);
            this.Controls.Add(this.myLabel5);
            this.Controls.Add(this.myLabel4);
            this.Controls.Add(this.myLabel3);
            this.Controls.Add(this.myLabel2);
            this.Controls.Add(this.myLabel1);
            this.Name = "bookingmanagement";
            this.Text = "bookingmanagement";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyControls.MyButton btnbook;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn crid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rntdt;
        private System.Windows.Forms.DataGridViewTextBoxColumn rtndt;
        private System.Windows.Forms.DataGridViewTextBoxColumn tamt;
        private MyControls.MyButton btnfind;
        private MyControls.MyButton btndelete;
        private MyControls.MyButton btnupdate;
        private MyControls.MyButton btnsave;
        private MyControls.MyTextBox myTextBox2;
        private MyControls.MyTextBox myTextBox1;
        private MyControls.MyComboBox myComboBox4;
        private MyControls.MyComboBox myComboBox3;
        private MyControls.MyComboBox myComboBox2;
        private MyControls.MyComboBox myComboBox1;
        private MyControls.MyLabel myLabel6;
        private MyControls.MyLabel myLabel5;
        private MyControls.MyLabel myLabel4;
        private MyControls.MyLabel myLabel3;
        private MyControls.MyLabel myLabel2;
        private MyControls.MyLabel myLabel1;
    }
}