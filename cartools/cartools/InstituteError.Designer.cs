﻿namespace cartools
{
    partial class InstituteError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstituteError));
            this.myButton1 = new MyControls.MyButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.myLabel1 = new MyControls.MyLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // myButton1
            // 
            this.myButton1.AutoSize = true;
            this.myButton1.BackColor = System.Drawing.Color.Red;
            this.myButton1.FlatAppearance.BorderSize = 2;
            this.myButton1.Font = new System.Drawing.Font("Comic Sans MS", 12F);
            this.myButton1.ForeColor = System.Drawing.Color.GhostWhite;
            this.myButton1.Location = new System.Drawing.Point(317, 355);
            this.myButton1.Name = "myButton1";
            this.myButton1.Size = new System.Drawing.Size(123, 45);
            this.myButton1.TabIndex = 0;
            this.myButton1.Text = "Cancel";
            this.myButton1.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(241, 76);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(278, 273);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // myLabel1
            // 
            this.myLabel1.AutoSize = true;
            this.myLabel1.BackColor = System.Drawing.Color.Transparent;
            this.myLabel1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myLabel1.ForeColor = System.Drawing.Color.Red;
            this.myLabel1.Location = new System.Drawing.Point(90, 14);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(190, 67);
            this.myLabel1.TabIndex = 2;
            this.myLabel1.Text = "ERROR";
            this.myLabel1.Click += new System.EventHandler(this.myLabel1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(81, 80);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // InstituteError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 423);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.myLabel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.myButton1);
            this.Name = "InstituteError";
            this.Text = "InstituteError";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyControls.MyButton myButton1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MyControls.MyLabel myLabel1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}