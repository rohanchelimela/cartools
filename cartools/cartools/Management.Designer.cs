﻿namespace cartools
{
    partial class Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ct = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.id1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.de = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnfind1 = new MyControls.MyButton();
            this.btndelete1 = new MyControls.MyButton();
            this.btnupdate1 = new MyControls.MyButton();
            this.btnsave1 = new MyControls.MyButton();
            this.myTextBox6 = new MyControls.MyTextBox();
            this.myTextBox5 = new MyControls.MyTextBox();
            this.myLabel8 = new MyControls.MyLabel();
            this.ctnm = new MyControls.MyLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fnm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lnm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dob = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnfind = new MyControls.MyButton();
            this.btndelete = new MyControls.MyButton();
            this.btnupdate = new MyControls.MyButton();
            this.btnsave = new MyControls.MyButton();
            this.myComboBox2 = new MyControls.MyComboBox();
            this.myComboBox1 = new MyControls.MyComboBox();
            this.myTextBox4 = new MyControls.MyTextBox();
            this.myTextBox3 = new MyControls.MyTextBox();
            this.myTextBox2 = new MyControls.MyTextBox();
            this.myTextBox1 = new MyControls.MyTextBox();
            this.cs = new System.Windows.Forms.TabControl();
            this.cm = new System.Windows.Forms.TabPage();
            this.myLabel6 = new MyControls.MyLabel();
            this.myLabel5 = new MyControls.MyLabel();
            this.myLabel4 = new MyControls.MyLabel();
            this.myLabel3 = new MyControls.MyLabel();
            this.myLabel2 = new MyControls.MyLabel();
            this.myLabel1 = new MyControls.MyLabel();
            this.userControl11 = new MyControls.UserControl1();
            this.ct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.cs.SuspendLayout();
            this.cm.SuspendLayout();
            this.SuspendLayout();
            // 
            // ct
            // 
            this.ct.Controls.Add(this.dataGridView2);
            this.ct.Controls.Add(this.btnfind1);
            this.ct.Controls.Add(this.btndelete1);
            this.ct.Controls.Add(this.btnupdate1);
            this.ct.Controls.Add(this.btnsave1);
            this.ct.Controls.Add(this.myTextBox6);
            this.ct.Controls.Add(this.myTextBox5);
            this.ct.Controls.Add(this.myLabel8);
            this.ct.Controls.Add(this.ctnm);
            this.ct.Location = new System.Drawing.Point(4, 22);
            this.ct.Name = "ct";
            this.ct.Padding = new System.Windows.Forms.Padding(3);
            this.ct.Size = new System.Drawing.Size(686, 518);
            this.ct.TabIndex = 1;
            this.ct.Text = "Category Management";
            this.ct.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Highlight;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id1,
            this.nm1,
            this.de});
            this.dataGridView2.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView2.Location = new System.Drawing.Point(122, 306);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(383, 150);
            this.dataGridView2.TabIndex = 8;
            // 
            // id1
            // 
            this.id1.HeaderText = "ID";
            this.id1.Name = "id1";
            // 
            // nm1
            // 
            this.nm1.HeaderText = "Name";
            this.nm1.Name = "nm1";
            // 
            // de
            // 
            this.de.HeaderText = "Description";
            this.de.Name = "de";
            // 
            // btnfind1
            // 
            this.btnfind1.AutoSize = true;
            this.btnfind1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnfind1.FlatAppearance.BorderSize = 2;
            this.btnfind1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnfind1.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnfind1.Location = new System.Drawing.Point(487, 228);
            this.btnfind1.Name = "btnfind1";
            this.btnfind1.Size = new System.Drawing.Size(96, 30);
            this.btnfind1.TabIndex = 7;
            this.btnfind1.Text = "Find";
            this.btnfind1.UseVisualStyleBackColor = false;
            // 
            // btndelete1
            // 
            this.btndelete1.AutoSize = true;
            this.btndelete1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btndelete1.FlatAppearance.BorderSize = 2;
            this.btndelete1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btndelete1.ForeColor = System.Drawing.Color.GhostWhite;
            this.btndelete1.Location = new System.Drawing.Point(487, 169);
            this.btndelete1.Name = "btndelete1";
            this.btndelete1.Size = new System.Drawing.Size(96, 30);
            this.btndelete1.TabIndex = 6;
            this.btndelete1.Text = "Delete";
            this.btndelete1.UseVisualStyleBackColor = false;
            // 
            // btnupdate1
            // 
            this.btnupdate1.AutoSize = true;
            this.btnupdate1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnupdate1.FlatAppearance.BorderSize = 2;
            this.btnupdate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnupdate1.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnupdate1.Location = new System.Drawing.Point(487, 111);
            this.btnupdate1.Name = "btnupdate1";
            this.btnupdate1.Size = new System.Drawing.Size(96, 30);
            this.btnupdate1.TabIndex = 5;
            this.btnupdate1.Text = "Update";
            this.btnupdate1.UseVisualStyleBackColor = false;
            // 
            // btnsave1
            // 
            this.btnsave1.AutoSize = true;
            this.btnsave1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnsave1.FlatAppearance.BorderSize = 2;
            this.btnsave1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnsave1.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnsave1.Location = new System.Drawing.Point(487, 57);
            this.btnsave1.Name = "btnsave1";
            this.btnsave1.Size = new System.Drawing.Size(96, 30);
            this.btnsave1.TabIndex = 4;
            this.btnsave1.Text = "Save";
            this.btnsave1.UseVisualStyleBackColor = false;
            // 
            // myTextBox6
            // 
            this.myTextBox6.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox6.Location = new System.Drawing.Point(198, 136);
            this.myTextBox6.Multiline = true;
            this.myTextBox6.Name = "myTextBox6";
            this.myTextBox6.Size = new System.Drawing.Size(224, 145);
            this.myTextBox6.TabIndex = 3;
            // 
            // myTextBox5
            // 
            this.myTextBox5.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox5.Location = new System.Drawing.Point(198, 57);
            this.myTextBox5.Name = "myTextBox5";
            this.myTextBox5.Size = new System.Drawing.Size(224, 29);
            this.myTextBox5.TabIndex = 2;
            // 
            // myLabel8
            // 
            this.myLabel8.AutoSize = true;
            this.myLabel8.BackColor = System.Drawing.Color.Transparent;
            this.myLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel8.Location = new System.Drawing.Point(60, 136);
            this.myLabel8.Name = "myLabel8";
            this.myLabel8.Size = new System.Drawing.Size(104, 24);
            this.myLabel8.TabIndex = 1;
            this.myLabel8.Text = "Description";
            // 
            // ctnm
            // 
            this.ctnm.AutoSize = true;
            this.ctnm.BackColor = System.Drawing.Color.Transparent;
            this.ctnm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.ctnm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.ctnm.Location = new System.Drawing.Point(60, 57);
            this.ctnm.Name = "ctnm";
            this.ctnm.Size = new System.Drawing.Size(61, 24);
            this.ctnm.TabIndex = 0;
            this.ctnm.Text = "Name";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Highlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.fnm,
            this.lnm,
            this.phno,
            this.lno,
            this.dob,
            this.iss});
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView1.Location = new System.Drawing.Point(50, 281);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(579, 226);
            this.dataGridView1.TabIndex = 35;
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            // 
            // fnm
            // 
            this.fnm.HeaderText = "First Name";
            this.fnm.Name = "fnm";
            // 
            // lnm
            // 
            this.lnm.HeaderText = "Last Name";
            this.lnm.Name = "lnm";
            // 
            // phno
            // 
            this.phno.HeaderText = "Phone Number";
            this.phno.Name = "phno";
            // 
            // lno
            // 
            this.lno.HeaderText = "Licence Number";
            this.lno.Name = "lno";
            // 
            // dob
            // 
            this.dob.HeaderText = "DOB";
            this.dob.Name = "dob";
            // 
            // iss
            // 
            this.iss.HeaderText = "Issue Date";
            this.iss.Name = "iss";
            // 
            // btnfind
            // 
            this.btnfind.AutoSize = true;
            this.btnfind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnfind.FlatAppearance.BorderSize = 2;
            this.btnfind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnfind.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnfind.Location = new System.Drawing.Point(516, 225);
            this.btnfind.Name = "btnfind";
            this.btnfind.Size = new System.Drawing.Size(96, 30);
            this.btnfind.TabIndex = 34;
            this.btnfind.Text = "Find";
            this.btnfind.UseVisualStyleBackColor = false;
            // 
            // btndelete
            // 
            this.btndelete.AutoSize = true;
            this.btndelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btndelete.FlatAppearance.BorderSize = 2;
            this.btndelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btndelete.ForeColor = System.Drawing.Color.GhostWhite;
            this.btndelete.Location = new System.Drawing.Point(379, 225);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(96, 30);
            this.btndelete.TabIndex = 33;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = false;
            // 
            // btnupdate
            // 
            this.btnupdate.AutoSize = true;
            this.btnupdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnupdate.FlatAppearance.BorderSize = 2;
            this.btnupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnupdate.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnupdate.Location = new System.Drawing.Point(233, 225);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(96, 30);
            this.btnupdate.TabIndex = 32;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = false;
            // 
            // btnsave
            // 
            this.btnsave.AutoSize = true;
            this.btnsave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnsave.FlatAppearance.BorderSize = 2;
            this.btnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnsave.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnsave.Location = new System.Drawing.Point(87, 225);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(96, 30);
            this.btnsave.TabIndex = 31;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = false;
            // 
            // myComboBox2
            // 
            this.myComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox2.FormattingEnabled = true;
            this.myComboBox2.Location = new System.Drawing.Point(484, 131);
            this.myComboBox2.Name = "myComboBox2";
            this.myComboBox2.Size = new System.Drawing.Size(145, 32);
            this.myComboBox2.TabIndex = 30;
            this.myComboBox2.Text = "--Select--";
            // 
            // myComboBox1
            // 
            this.myComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox1.FormattingEnabled = true;
            this.myComboBox1.Location = new System.Drawing.Point(168, 131);
            this.myComboBox1.Name = "myComboBox1";
            this.myComboBox1.Size = new System.Drawing.Size(145, 32);
            this.myComboBox1.TabIndex = 29;
            this.myComboBox1.Text = "--Select--";
            // 
            // myTextBox4
            // 
            this.myTextBox4.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox4.Location = new System.Drawing.Point(484, 76);
            this.myTextBox4.Name = "myTextBox4";
            this.myTextBox4.Size = new System.Drawing.Size(145, 29);
            this.myTextBox4.TabIndex = 28;
            // 
            // myTextBox3
            // 
            this.myTextBox3.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox3.Location = new System.Drawing.Point(484, 27);
            this.myTextBox3.Name = "myTextBox3";
            this.myTextBox3.Size = new System.Drawing.Size(145, 29);
            this.myTextBox3.TabIndex = 27;
            // 
            // myTextBox2
            // 
            this.myTextBox2.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox2.Location = new System.Drawing.Point(168, 81);
            this.myTextBox2.Name = "myTextBox2";
            this.myTextBox2.Size = new System.Drawing.Size(145, 29);
            this.myTextBox2.TabIndex = 26;
            // 
            // myTextBox1
            // 
            this.myTextBox1.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox1.Location = new System.Drawing.Point(168, 29);
            this.myTextBox1.Name = "myTextBox1";
            this.myTextBox1.Size = new System.Drawing.Size(145, 29);
            this.myTextBox1.TabIndex = 25;
            // 
            // cs
            // 
            this.cs.Controls.Add(this.cm);
            this.cs.Controls.Add(this.ct);
            this.cs.Location = new System.Drawing.Point(-6, 3);
            this.cs.Name = "cs";
            this.cs.SelectedIndex = 0;
            this.cs.Size = new System.Drawing.Size(694, 544);
            this.cs.TabIndex = 1;
            // 
            // cm
            // 
            this.cm.Controls.Add(this.dataGridView1);
            this.cm.Controls.Add(this.btnfind);
            this.cm.Controls.Add(this.btndelete);
            this.cm.Controls.Add(this.btnupdate);
            this.cm.Controls.Add(this.btnsave);
            this.cm.Controls.Add(this.myComboBox2);
            this.cm.Controls.Add(this.myComboBox1);
            this.cm.Controls.Add(this.myTextBox4);
            this.cm.Controls.Add(this.myTextBox3);
            this.cm.Controls.Add(this.myTextBox2);
            this.cm.Controls.Add(this.myTextBox1);
            this.cm.Controls.Add(this.myLabel6);
            this.cm.Controls.Add(this.myLabel5);
            this.cm.Controls.Add(this.myLabel4);
            this.cm.Controls.Add(this.myLabel3);
            this.cm.Controls.Add(this.myLabel2);
            this.cm.Controls.Add(this.myLabel1);
            this.cm.Controls.Add(this.userControl11);
            this.cm.Location = new System.Drawing.Point(4, 22);
            this.cm.Name = "cm";
            this.cm.Padding = new System.Windows.Forms.Padding(3);
            this.cm.Size = new System.Drawing.Size(686, 518);
            this.cm.TabIndex = 0;
            this.cm.Text = "Customer Management";
            this.cm.UseVisualStyleBackColor = true;
            // 
            // myLabel6
            // 
            this.myLabel6.AutoSize = true;
            this.myLabel6.BackColor = System.Drawing.Color.Transparent;
            this.myLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel6.Location = new System.Drawing.Point(361, 139);
            this.myLabel6.Name = "myLabel6";
            this.myLabel6.Size = new System.Drawing.Size(97, 24);
            this.myLabel6.TabIndex = 24;
            this.myLabel6.Text = "Issue Date";
            // 
            // myLabel5
            // 
            this.myLabel5.AutoSize = true;
            this.myLabel5.BackColor = System.Drawing.Color.Transparent;
            this.myLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel5.Location = new System.Drawing.Point(361, 81);
            this.myLabel5.Name = "myLabel5";
            this.myLabel5.Size = new System.Drawing.Size(77, 24);
            this.myLabel5.TabIndex = 23;
            this.myLabel5.Text = "Licence";
            // 
            // myLabel4
            // 
            this.myLabel4.AutoSize = true;
            this.myLabel4.BackColor = System.Drawing.Color.Transparent;
            this.myLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel4.Location = new System.Drawing.Point(361, 32);
            this.myLabel4.Name = "myLabel4";
            this.myLabel4.Size = new System.Drawing.Size(102, 24);
            this.myLabel4.TabIndex = 22;
            this.myLabel4.Text = "Last NAme";
            // 
            // myLabel3
            // 
            this.myLabel3.AutoSize = true;
            this.myLabel3.BackColor = System.Drawing.Color.Transparent;
            this.myLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel3.Location = new System.Drawing.Point(46, 139);
            this.myLabel3.Name = "myLabel3";
            this.myLabel3.Size = new System.Drawing.Size(114, 24);
            this.myLabel3.TabIndex = 21;
            this.myLabel3.Text = "Date Of Birth";
            // 
            // myLabel2
            // 
            this.myLabel2.AutoSize = true;
            this.myLabel2.BackColor = System.Drawing.Color.Transparent;
            this.myLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel2.Location = new System.Drawing.Point(46, 81);
            this.myLabel2.Name = "myLabel2";
            this.myLabel2.Size = new System.Drawing.Size(101, 24);
            this.myLabel2.TabIndex = 20;
            this.myLabel2.Text = "Phone No.";
            // 
            // myLabel1
            // 
            this.myLabel1.AutoSize = true;
            this.myLabel1.BackColor = System.Drawing.Color.Transparent;
            this.myLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel1.Location = new System.Drawing.Point(46, 32);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(101, 24);
            this.myLabel1.TabIndex = 19;
            this.myLabel1.Text = "First Name";
            // 
            // userControl11
            // 
            this.userControl11.Location = new System.Drawing.Point(199, 151);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(150, 150);
            this.userControl11.TabIndex = 18;
            // 
            // Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 524);
            this.Controls.Add(this.cs);
            this.Name = "Management";
            this.Text = "Management";
            this.ct.ResumeLayout(false);
            this.ct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.cs.ResumeLayout(false);
            this.cm.ResumeLayout(false);
            this.cm.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage ct;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn id1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm1;
        private System.Windows.Forms.DataGridViewTextBoxColumn de;
        private MyControls.MyButton btnfind1;
        private MyControls.MyButton btndelete1;
        private MyControls.MyButton btnupdate1;
        private MyControls.MyButton btnsave1;
        private MyControls.MyTextBox myTextBox6;
        private MyControls.MyTextBox myTextBox5;
        private MyControls.MyLabel myLabel8;
        private MyControls.MyLabel ctnm;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fnm;
        private System.Windows.Forms.DataGridViewTextBoxColumn lnm;
        private System.Windows.Forms.DataGridViewTextBoxColumn phno;
        private System.Windows.Forms.DataGridViewTextBoxColumn lno;
        private System.Windows.Forms.DataGridViewTextBoxColumn dob;
        private System.Windows.Forms.DataGridViewTextBoxColumn iss;
        private MyControls.MyButton btnfind;
        private MyControls.MyButton btndelete;
        private MyControls.MyButton btnupdate;
        private MyControls.MyButton btnsave;
        private MyControls.MyComboBox myComboBox2;
        private MyControls.MyComboBox myComboBox1;
        private MyControls.MyTextBox myTextBox4;
        private MyControls.MyTextBox myTextBox3;
        private MyControls.MyTextBox myTextBox2;
        private MyControls.MyTextBox myTextBox1;
        private System.Windows.Forms.TabControl cs;
        private System.Windows.Forms.TabPage cm;
        private MyControls.MyLabel myLabel6;
        private MyControls.MyLabel myLabel5;
        private MyControls.MyLabel myLabel4;
        private MyControls.MyLabel myLabel3;
        private MyControls.MyLabel myLabel2;
        private MyControls.MyLabel myLabel1;
        private MyControls.UserControl1 userControl11;
    }
}