﻿namespace cartools
{
    partial class carmanagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mfd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ins = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ml = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnfind = new MyControls.MyButton();
            this.btndelete = new MyControls.MyButton();
            this.btnupdate = new MyControls.MyButton();
            this.btnsave = new MyControls.MyButton();
            this.myComboBox2 = new MyControls.MyComboBox();
            this.myComboBox1 = new MyControls.MyComboBox();
            this.myTextBox9 = new MyControls.MyTextBox();
            this.myTextBox6 = new MyControls.MyTextBox();
            this.myTextBox5 = new MyControls.MyTextBox();
            this.myTextBox4 = new MyControls.MyTextBox();
            this.myTextBox3 = new MyControls.MyTextBox();
            this.myTextBox2 = new MyControls.MyTextBox();
            this.myTextBox1 = new MyControls.MyTextBox();
            this.myLabel9 = new MyControls.MyLabel();
            this.myLabel8 = new MyControls.MyLabel();
            this.myLabel7 = new MyControls.MyLabel();
            this.myLabel6 = new MyControls.MyLabel();
            this.myLabel5 = new MyControls.MyLabel();
            this.myLabel4 = new MyControls.MyLabel();
            this.myLabel3 = new MyControls.MyLabel();
            this.myLabel2 = new MyControls.MyLabel();
            this.myLabel1 = new MyControls.MyLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Highlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.ct,
            this.nm,
            this.no,
            this.dp,
            this.mfd,
            this.cl,
            this.ins,
            this.ml});
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView1.Location = new System.Drawing.Point(86, 311);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(545, 150);
            this.dataGridView1.TabIndex = 47;
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            // 
            // ct
            // 
            this.ct.HeaderText = "Category";
            this.ct.Name = "ct";
            // 
            // nm
            // 
            this.nm.HeaderText = "Name";
            this.nm.Name = "nm";
            // 
            // no
            // 
            this.no.HeaderText = "Number";
            this.no.Name = "no";
            // 
            // dp
            // 
            this.dp.HeaderText = "Daily Price";
            this.dp.Name = "dp";
            // 
            // mfd
            // 
            this.mfd.HeaderText = "Mfd Date";
            this.mfd.Name = "mfd";
            // 
            // cl
            // 
            this.cl.HeaderText = "Color";
            this.cl.Name = "cl";
            // 
            // ins
            // 
            this.ins.HeaderText = "Insurance Number";
            this.ins.Name = "ins";
            // 
            // ml
            // 
            this.ml.HeaderText = "Milage";
            this.ml.Name = "ml";
            // 
            // btnfind
            // 
            this.btnfind.AutoSize = true;
            this.btnfind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnfind.FlatAppearance.BorderSize = 2;
            this.btnfind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnfind.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnfind.Location = new System.Drawing.Point(535, 265);
            this.btnfind.Name = "btnfind";
            this.btnfind.Size = new System.Drawing.Size(96, 30);
            this.btnfind.TabIndex = 46;
            this.btnfind.Text = "Find";
            this.btnfind.UseVisualStyleBackColor = false;
            // 
            // btndelete
            // 
            this.btndelete.AutoSize = true;
            this.btndelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btndelete.FlatAppearance.BorderSize = 2;
            this.btndelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btndelete.ForeColor = System.Drawing.Color.GhostWhite;
            this.btndelete.Location = new System.Drawing.Point(385, 265);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(96, 30);
            this.btndelete.TabIndex = 45;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = false;
            // 
            // btnupdate
            // 
            this.btnupdate.AutoSize = true;
            this.btnupdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnupdate.FlatAppearance.BorderSize = 2;
            this.btnupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnupdate.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnupdate.Location = new System.Drawing.Point(231, 265);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(96, 30);
            this.btnupdate.TabIndex = 44;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = false;
            // 
            // btnsave
            // 
            this.btnsave.AutoSize = true;
            this.btnsave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.btnsave.FlatAppearance.BorderSize = 2;
            this.btnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnsave.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnsave.Location = new System.Drawing.Point(86, 265);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(96, 30);
            this.btnsave.TabIndex = 43;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = false;
            // 
            // myComboBox2
            // 
            this.myComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox2.FormattingEnabled = true;
            this.myComboBox2.Location = new System.Drawing.Point(486, 135);
            this.myComboBox2.Name = "myComboBox2";
            this.myComboBox2.Size = new System.Drawing.Size(145, 32);
            this.myComboBox2.TabIndex = 42;
            this.myComboBox2.Text = "--Select--";
            // 
            // myComboBox1
            // 
            this.myComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myComboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myComboBox1.FormattingEnabled = true;
            this.myComboBox1.Location = new System.Drawing.Point(182, 68);
            this.myComboBox1.Name = "myComboBox1";
            this.myComboBox1.Size = new System.Drawing.Size(145, 32);
            this.myComboBox1.TabIndex = 41;
            this.myComboBox1.Text = "--Select--";
            // 
            // myTextBox9
            // 
            this.myTextBox9.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox9.Location = new System.Drawing.Point(486, 198);
            this.myTextBox9.Name = "myTextBox9";
            this.myTextBox9.Size = new System.Drawing.Size(145, 29);
            this.myTextBox9.TabIndex = 40;
            // 
            // myTextBox6
            // 
            this.myTextBox6.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox6.Location = new System.Drawing.Point(486, 80);
            this.myTextBox6.Name = "myTextBox6";
            this.myTextBox6.Size = new System.Drawing.Size(145, 29);
            this.myTextBox6.TabIndex = 39;
            // 
            // myTextBox5
            // 
            this.myTextBox5.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox5.Location = new System.Drawing.Point(182, 209);
            this.myTextBox5.Name = "myTextBox5";
            this.myTextBox5.Size = new System.Drawing.Size(145, 29);
            this.myTextBox5.TabIndex = 38;
            // 
            // myTextBox4
            // 
            this.myTextBox4.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox4.Location = new System.Drawing.Point(486, 25);
            this.myTextBox4.Name = "myTextBox4";
            this.myTextBox4.Size = new System.Drawing.Size(145, 29);
            this.myTextBox4.TabIndex = 37;
            // 
            // myTextBox3
            // 
            this.myTextBox3.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox3.Location = new System.Drawing.Point(182, 117);
            this.myTextBox3.Name = "myTextBox3";
            this.myTextBox3.Size = new System.Drawing.Size(145, 29);
            this.myTextBox3.TabIndex = 36;
            // 
            // myTextBox2
            // 
            this.myTextBox2.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox2.Location = new System.Drawing.Point(182, 165);
            this.myTextBox2.Name = "myTextBox2";
            this.myTextBox2.Size = new System.Drawing.Size(145, 29);
            this.myTextBox2.TabIndex = 35;
            // 
            // myTextBox1
            // 
            this.myTextBox1.BackColor = System.Drawing.Color.GhostWhite;
            this.myTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myTextBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myTextBox1.Location = new System.Drawing.Point(182, 25);
            this.myTextBox1.Name = "myTextBox1";
            this.myTextBox1.Size = new System.Drawing.Size(145, 29);
            this.myTextBox1.TabIndex = 34;
            // 
            // myLabel9
            // 
            this.myLabel9.AutoSize = true;
            this.myLabel9.BackColor = System.Drawing.Color.Transparent;
            this.myLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel9.Location = new System.Drawing.Point(356, 203);
            this.myLabel9.Name = "myLabel9";
            this.myLabel9.Size = new System.Drawing.Size(128, 24);
            this.myLabel9.TabIndex = 33;
            this.myLabel9.Text = "Insurance No.";
            // 
            // myLabel8
            // 
            this.myLabel8.AutoSize = true;
            this.myLabel8.BackColor = System.Drawing.Color.Transparent;
            this.myLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel8.Location = new System.Drawing.Point(356, 143);
            this.myLabel8.Name = "myLabel8";
            this.myLabel8.Size = new System.Drawing.Size(84, 24);
            this.myLabel8.TabIndex = 32;
            this.myLabel8.Text = "Mfd Date";
            // 
            // myLabel7
            // 
            this.myLabel7.AutoSize = true;
            this.myLabel7.BackColor = System.Drawing.Color.Transparent;
            this.myLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel7.Location = new System.Drawing.Point(60, 214);
            this.myLabel7.Name = "myLabel7";
            this.myLabel7.Size = new System.Drawing.Size(66, 24);
            this.myLabel7.TabIndex = 31;
            this.myLabel7.Text = "Milage";
            // 
            // myLabel6
            // 
            this.myLabel6.AutoSize = true;
            this.myLabel6.BackColor = System.Drawing.Color.Transparent;
            this.myLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel6.Location = new System.Drawing.Point(356, 30);
            this.myLabel6.Name = "myLabel6";
            this.myLabel6.Size = new System.Drawing.Size(113, 24);
            this.myLabel6.TabIndex = 30;
            this.myLabel6.Text = "Car Number";
            // 
            // myLabel5
            // 
            this.myLabel5.AutoSize = true;
            this.myLabel5.BackColor = System.Drawing.Color.Transparent;
            this.myLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel5.Location = new System.Drawing.Point(356, 85);
            this.myLabel5.Name = "myLabel5";
            this.myLabel5.Size = new System.Drawing.Size(116, 24);
            this.myLabel5.TabIndex = 29;
            this.myLabel5.Text = "Brand Name";
            // 
            // myLabel4
            // 
            this.myLabel4.AutoSize = true;
            this.myLabel4.BackColor = System.Drawing.Color.Transparent;
            this.myLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel4.Location = new System.Drawing.Point(60, 168);
            this.myLabel4.Name = "myLabel4";
            this.myLabel4.Size = new System.Drawing.Size(55, 24);
            this.myLabel4.TabIndex = 28;
            this.myLabel4.Text = "Color";
            // 
            // myLabel3
            // 
            this.myLabel3.AutoSize = true;
            this.myLabel3.BackColor = System.Drawing.Color.Transparent;
            this.myLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel3.Location = new System.Drawing.Point(60, 76);
            this.myLabel3.Name = "myLabel3";
            this.myLabel3.Size = new System.Drawing.Size(85, 24);
            this.myLabel3.TabIndex = 27;
            this.myLabel3.Text = "Category";
            // 
            // myLabel2
            // 
            this.myLabel2.AutoSize = true;
            this.myLabel2.BackColor = System.Drawing.Color.Transparent;
            this.myLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel2.Location = new System.Drawing.Point(60, 122);
            this.myLabel2.Name = "myLabel2";
            this.myLabel2.Size = new System.Drawing.Size(98, 24);
            this.myLabel2.TabIndex = 26;
            this.myLabel2.Text = "Daily Price";
            // 
            // myLabel1
            // 
            this.myLabel1.AutoSize = true;
            this.myLabel1.BackColor = System.Drawing.Color.Transparent;
            this.myLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.myLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(166)))), ((int)(((byte)(255)))));
            this.myLabel1.Location = new System.Drawing.Point(60, 30);
            this.myLabel1.Name = "myLabel1";
            this.myLabel1.Size = new System.Drawing.Size(95, 24);
            this.myLabel1.TabIndex = 25;
            this.myLabel1.Text = "Car Name";
            // 
            // carmanagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 479);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnfind);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.myComboBox2);
            this.Controls.Add(this.myComboBox1);
            this.Controls.Add(this.myTextBox9);
            this.Controls.Add(this.myTextBox6);
            this.Controls.Add(this.myTextBox5);
            this.Controls.Add(this.myTextBox4);
            this.Controls.Add(this.myTextBox3);
            this.Controls.Add(this.myTextBox2);
            this.Controls.Add(this.myTextBox1);
            this.Controls.Add(this.myLabel9);
            this.Controls.Add(this.myLabel8);
            this.Controls.Add(this.myLabel7);
            this.Controls.Add(this.myLabel6);
            this.Controls.Add(this.myLabel5);
            this.Controls.Add(this.myLabel4);
            this.Controls.Add(this.myLabel3);
            this.Controls.Add(this.myLabel2);
            this.Controls.Add(this.myLabel1);
            this.Name = "carmanagement";
            this.Text = "carmanagement";
            this.Load += new System.EventHandler(this.carmanagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ct;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm;
        private System.Windows.Forms.DataGridViewTextBoxColumn no;
        private System.Windows.Forms.DataGridViewTextBoxColumn dp;
        private System.Windows.Forms.DataGridViewTextBoxColumn mfd;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl;
        private System.Windows.Forms.DataGridViewTextBoxColumn ins;
        private System.Windows.Forms.DataGridViewTextBoxColumn ml;
        private MyControls.MyButton btnfind;
        private MyControls.MyButton btndelete;
        private MyControls.MyButton btnupdate;
        private MyControls.MyButton btnsave;
        private MyControls.MyComboBox myComboBox2;
        private MyControls.MyComboBox myComboBox1;
        private MyControls.MyTextBox myTextBox9;
        private MyControls.MyTextBox myTextBox6;
        private MyControls.MyTextBox myTextBox5;
        private MyControls.MyTextBox myTextBox4;
        private MyControls.MyTextBox myTextBox3;
        private MyControls.MyTextBox myTextBox2;
        private MyControls.MyTextBox myTextBox1;
        private MyControls.MyLabel myLabel9;
        private MyControls.MyLabel myLabel8;
        private MyControls.MyLabel myLabel7;
        private MyControls.MyLabel myLabel6;
        private MyControls.MyLabel myLabel5;
        private MyControls.MyLabel myLabel4;
        private MyControls.MyLabel myLabel3;
        private MyControls.MyLabel myLabel2;
        private MyControls.MyLabel myLabel1;
    }
}